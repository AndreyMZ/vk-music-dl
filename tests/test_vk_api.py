from typing import Type

import pytest # type: ignore
import vk_api # type: ignore

from vk_music_dl.__main__ import CONFIG_PATH


@pytest.mark.xfail
def test_vk_api_original():
	_test_vk_api(vk_api.VkApi)

def test_vk_api_patched():
	import vk_music_dl.vk_api_patched
	_test_vk_api(vk_music_dl.vk_api_patched.VkApi)

def _test_vk_api(vk_api_class: Type[vk_api.VkApi]) -> None:
	assert not CONFIG_PATH.exists()
	session = vk_api_class(login="foo")
	with pytest.raises(vk_api.PasswordRequired):
		session.auth(token_only=True)
