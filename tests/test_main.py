import sys

import pytest # type: ignore

from vk_music_dl.__main__ import encode_filename


@pytest.mark.skipif(sys.platform != 'win32', reason="Linux only")
def test_encode_filename_windows():
	assert encode_filename("Foo \"Bar\"") == "Foo Bar"
	assert encode_filename("foo\0bar") == "foobar"

@pytest.mark.skipif(sys.platform == 'win32', reason="Windows only")
def test_encode_filename_linux():
	assert encode_filename("Foo \"Bar\"") == "Foo \"Bar\""
	assert encode_filename("foo\0bar") == "foobar"
