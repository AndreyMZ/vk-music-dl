import logging
import sys


def configure_logging(debug: bool = False) -> None:
	logging.addLevelName(logging.CRITICAL, "FATAL")
	logging.addLevelName(logging.WARNING,  "WARN")

	logging.basicConfig(
		style='{',
		format="{asctime} {levelname:5} [{name}] {message}",
		level=(logging.DEBUG if debug else logging.INFO),
		stream=sys.stderr,
	)
