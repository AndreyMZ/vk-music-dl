from argparse import Action, HelpFormatter, OPTIONAL, SUPPRESS, ZERO_OR_MORE
from gettext import gettext as _
from subprocess import list2cmdline
from typing import Optional


class MyHelpFormatter(HelpFormatter):
	def _get_help_string(self, action: Action) -> Optional[str]:
		if (action.help is not None) and ("%(default)" not in action.help):
			if action.default is SUPPRESS:
				pass
			elif action.default is None:
				if action.required:
					return action.help + _(" [Required]")
				else:
					if action.option_strings or action.nargs in [OPTIONAL, ZERO_OR_MORE]:
						return action.help + _(" [Optional]")
					else:
						pass
			elif isinstance(action.default, list):
				return action.help + _(" [Default: %s]") % list2cmdline(list(map(str, action.default)))
			else:
				return action.help + _(" [Default: %s]") % list2cmdline([str(action.default)])
		return action.help
