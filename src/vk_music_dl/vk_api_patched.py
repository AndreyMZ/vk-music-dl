import vk_api # type: ignore


class VkApi(vk_api.VkApi):
	def _auth_token(self, reauth: bool = False) -> None:
		if reauth:
			self.logger.info('Auth (API) forced')

		if not reauth and self._check_token():
			self.logger.info('access_token from config is valid')
		elif self.check_sid():
			self._pass_security_check()
			self._api_login()
		else:
			self._vk_login()
			self._api_login()
