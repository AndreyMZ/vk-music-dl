import argparse
import json
import logging
import os
import re
import subprocess
import sys
from getpass import getpass
from gettext import gettext as _
from pathlib import Path
from typing import Any, TypedDict, cast

import mutagen as mutagen  # type: ignore
import vk_api  # type: ignore
import vk_api.audio  # type: ignore

from .utils.argparse import MyHelpFormatter
from .utils.logging import configure_logging
from .vk_api_patched import VkApi


class Track(TypedDict):
	id: int
	owner_id: int
	track_covers: list[Any]
	url: str
	artist: str
	title: str
	duration: float


URL_PATTERNS = [
	# User's audios.
	re.compile(r"https://vk\.com/audios(?P<owner>[\-\d]+)"),
	# User's own album or playlist (pop-up).
	re.compile(r"https://vk\.com/audios(?P<owner>[\-\d]+)\?z=audio_playlist\1_(?P<album>[\-\d]+)"),
	# User's third-party album or playlist (pop-up).
	re.compile(r"https://vk\.com/audios[\-\d]+\?z=audio_playlist(?P<owner>[\-\d]+)_(?P<album>[\-\d]+)%2F(?P<access_hash>[0-9a-f]+)"),
	# Album
	re.compile(r"https://vk\.com/music/album/(?P<owner>[\-\d]+)_(?P<album>[\-\d]+)_(?P<access_hash>[0-9a-f]+)"),
	# Playlist
	re.compile(r"https://vk\.com/music/playlist/(?P<owner>[\-\d]+)_(?P<album>[\-\d]+)_(?P<access_hash>[0-9a-f]+)"),
]

CONFIG_PATH = Path("vk_config.v2.json")


def main() -> None:
	os.environ.setdefault("COLUMNS", "120")
	parser = argparse.ArgumentParser(formatter_class=MyHelpFormatter, prog=f"vk-music-dl")
	parser.add_argument("url", type=str, help=_("URL of audios, an album, or a playlist in VK."))
	parser.add_argument("--user", type=str, required=False, help=_("Name of user in VK."))
	parser.add_argument("--format", type=str, default="{number:02}. {artist} - {title}", help=_("Format of file names."))
	parser.add_argument("--debug", default=False, action='store_true', help=_("Enable debug logs."))
	parser.add_argument("--dry-run", default=False, action='store_true', help=_("Do not actually download the files."))
	args = parser.parse_args()

	configure_logging(debug=args.debug)
	logger = logging.getLogger(__package__)

	filename_format = f"{args.format}.mp3"
	try:
		filename_format.format(number=1, artist="", title="")
	except LookupError as ex:
		logger.error(f"Unsupported format placeholder: {ex}.")
		return

	match = next(filter(None, (regex.fullmatch(args.url) for regex in URL_PATTERNS)), None)
	if match is None:
		logger.error("The URL is not supported. Supported URL patterns:\n%s",
		              "\n".join(regex.pattern for regex in URL_PATTERNS))
		return
	groups = match.groupdict()
	owner_id = groups.get("owner")
	album_id = groups.get("album")
	access_hash = groups.get("access_hash")

	# Get username.
	username: str
	if args.user is not None:
		username = args.user
	elif CONFIG_PATH.exists():
		with CONFIG_PATH.open('rt') as file:
			config: dict[str, Any] = json.load(file)
		username = next(key for key in config)
	else:
		print(f"Username: ", end="", flush=True)
		username = sys.stdin.readline().rstrip("\n")

	# Try to authenticate without a password (with token); ask the password if required.
	session = VkApi(login=username)
	try:
		session.auth(token_only=True)
	except vk_api.PasswordRequired:
		password = getpass("Password: ")
		session = VkApi(login=username, password=password)
		session.auth(token_only=True)

	vk_audio = vk_api.audio.VkAudio(session)
	dirname = "_".join(map(str, filter(None, [owner_id, album_id, access_hash])))
	for i, track in enumerate(vk_audio.get_iter(owner_id, album_id, access_hash), start=1):
		track = cast(Track, track)
		filename = filename_format.format(
			number=i,
			artist=encode_filename(track['artist']).strip(),
			title=encode_filename(track['title']).strip(),
		)
		filepath = Path(dirname, filename)
		print(f"\nDownloading \"{filepath}\"...")
		logger.debug("Downloading from: %s", track['url'])

		if not args.dry_run:
			# Download audio.
			subprocess.check_call(["yt-dlp", "--hls-use-mpegts", "--extract-audio", "--audio-format=mp3",
			                       "--output", filepath, "--", track['url']])

			# Write audio tags.
			audio_tags = mutagen.File(filepath, easy=True)
			audio_tags['tracknumber'] = str(i)
			audio_tags['artist'] = track['artist']
			audio_tags['title']  = track['title']
			audio_tags.save()


FILENAME_TRANSLATION = str.maketrans({c: None for c in ("\0/\\:*?\"<>|" if sys.platform == 'win32' else
                                                        "\0/")})
def encode_filename(text: str) -> str:
	return text.translate(FILENAME_TRANSLATION)


if __name__ == '__main__':
	main()
