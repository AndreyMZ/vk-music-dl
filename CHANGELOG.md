## 0.2.0

2022-08-27

- Fill audio (ID3v2) tags in audio (MP3) files.

## 0.1.0 

2022-08-26

- Initial release.
