Requirements
============

- [Python][] 3.10+
- [yt-dlp][]
- [ffmpeg][]

## Python

On Windows:
1. Install [Chocolatey][].
2. Run:
    
        choco install python

## yt-dlp

1. Install [pipx][].
2. Run:

        pipx install yt-dlp

## ffmpeg

On Windows:

1. Install [Chocolatey][].
2. Run:
    
        choco install ffmpeg


Installation
============

1. Install [pipx][].
2. Run:
    
        pipx install vk-music-dl --index-url https://gitlab.com/api/v4/projects/AndreyMZ%2Fvk-music-dl/packages/pypi/simple


Usage
=====

    usage: vk-music-dl [-h] [--debug] [--user USER] [--format FORMAT] url
    
    positional arguments:
      url              URL of audios, an album, or a playlist in VK. [Required]
    
    options:
      -h, --help       show this help message and exit
      --user USER      Name of user in VK. [Optional]
      --format FORMAT  Format of file names. [Default: "{number:02}. {artist} - {title}"]
      --debug          Enable debug logs. [Default: False]
      --dry-run        Do not actually download the files. [Default: False]

Supported URL patterns:

- `https://vk\.com/audios(?P<owner>[\-\d]+)`
- `https://vk\.com/audios(?P<owner>[\-\d]+)\?z=audio_playlist\1_(?P<album>[\-\d]+)`
- `https://vk\.com/audios[\-\d]+\?z=audio_playlist(?P<owner>[\-\d]+)_(?P<album>[\-\d]+)%%2F(?P<access_hash>[0-9a-f]+)`
- `https://vk\.com/music/album/(?P<owner>[\-\d]+)_(?P<album>[\-\d]+)_(?P<access_hash>[0-9a-f]+)`
- `https://vk\.com/music/playlist/(?P<owner>[\-\d]+)_(?P<album>[\-\d]+)_(?P<access_hash>[0-9a-f]+)`


[Python]: https://www.python.org/
[yt-dlp]: https://github.com/yt-dlp/yt-dlp#readme
[ffmpeg]: https://ffmpeg.org
[Chocolatey]: https://chocolatey.org/install
[pipx]: https://pypa.github.io/pipx/#install-pipx
